#!/usr/bin/env fish

# source: https://gist.github.com/moskyb/b90844c32c3d065878854d7d5e425df1
function checkout-fork
    argparse h/help 'r/repo=' 'a/account=' 'b/branch=' -- $argv

    if string match -r -- '-?-h(elp)?' $_flag_help
        echo "Usage: checkout-fork --account <fork account> [args]"
        echo
        echo "Checks out a fork of the repo you're currently in, adding a remote, and checking out the branch of the fork"
        echo
        echo "Options:"
        echo "  -h/--help: Show this help text"
        echo "  -a/--account: Set the account name of the fork. This must be set"
        echo "  -r/--repo: Set the repository name of the fork. Defaults to the name of repository this command is called from"
        echo "  -b/--branch: Set the branch name of the fork. Defaults to 'main'"
        return 0
    end

    set repo (basename (git rev-parse --show-toplevel))
    if test -n $_flag_repo
        set repo $_flag_repo
    end

    if test -z $_flag_account
        echo "Account flag is mandatory, use either --account or -a"
        return 1
    end
    set fork_account $_flag_account

    set fork_branch main
    if test -n $_flag_branch
        set fork_branch $_flag_branch
    end

    set fish_trace 1
    echo git remote add $fork_account "git@github.com:$fork_account/$repo.git"; or return
    git fetch $fork_account; or return
    git checkout --track $fork_account/$fork_branch; or return
    set fish_trace 0
end

checkout-fork $argv
