import atexit
import os
import readline
import rlcompleter
import sys

readline.parse_and_bind("tab: complete")

histfile = os.path.join(os.environ["XDG_CONFIG_HOME"], "python", "history")

try:
    readline.read_history_file(histfile)
    h_len = readline.get_current_history_length()
except FileNotFoundError:
    open(histfile, "wb").close()
    h_len = 0


def save(prev_h_len, histfile):
    import readline, sys

    readline.set_history_length(1000)
    if sys.version_info[0] >= 3:
        new_h_len = readline.get_current_history_length()
        readline.append_history_file(new_h_len - prev_h_len, histfile)
    else:
        readline.write_history_file(histfile)


atexit.register(save, h_len, histfile)
del os, histfile, readline, rlcompleter, atexit, sys
