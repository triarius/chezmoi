#!/usr/bin/env ruby

# frozen_string_literal: true

require "json"
require "net/http"

WEATHER_CODES = {
  "113" => "☀️ ",
  "116" => "⛅️",
  "119" => "☁️",
  "122" => "☁️",
  "143" => "🌫",
  "176" => "🌦",
  "179" => "🌧",
  "182" => "🌧",
  "185" => "🌧",
  "200" => "⛈",
  "227" => "🌨",
  "230" => "❄️",
  "248" => "🌫",
  "260" => "🌫",
  "263" => "🌦",
  "266" => "🌦",
  "281" => "🌧",
  "284" => "🌧",
  "293" => "🌦",
  "296" => "🌦",
  "299" => "🌧",
  "302" => "🌧",
  "305" => "🌧",
  "308" => "🌧",
  "311" => "🌧",
  "314" => "🌧",
  "317" => "🌧",
  "320" => "🌨",
  "323" => "🌨",
  "326" => "🌨",
  "329" => "❄️",
  "332" => "❄️",
  "335" => "❄️",
  "338" => "❄️",
  "350" => "🌧",
  "353" => "🌦",
  "356" => "🌧",
  "359" => "🌧",
  "362" => "🌧",
  "365" => "🌧",
  "368" => "🌨",
  "371" => "❄️",
  "374" => "🌧",
  "377" => "🌧",
  "386" => "⛈",
  "389" => "🌩",
  "392" => "⛈",
  "395" => "❄️",
}

CHANCES = {
  "chanceoffog": "Fog",
  "chanceoffrost": "Frost",
  "chanceofovercast": "Overcast",
  "chanceofrain": "Rain",
  "chanceofsnow": "Snow",
  "chanceofsunshine": "Sunshine",
  "chanceofthunder": "Thunder",
  "chanceofwindy": "Wind",
}

def format_chances(hour)
  hour.to_json

  # conditions = []
  # for event in chances.keys():
  #     if int(hour[event]) > 0:
  #         conditions.append(chances[event] + " " + hour[event] + "%")
  # return ", ".join(conditions)
end

def format_time(time)
  time
  ## return time.replace("00", "").zfill(2)
end

def format_temp(hour)
  hour
  #   return (hour["FeelsLikeC"] + "°").ljust(3)
end

def get_follow_redirects(url:, count: 0)
  raise "Too many redirects" if count > 10

  resp = Net::HTTP.get_response URI.parse(url)
  if resp.code == "301"
    get_follow_redirects(resp["location"], count += 1)
  else
    resp
  end
end

URL = "https://wttr.in/?format=j1"
resp = get_follow_redirects(url: URL)
weather = JSON.parse(resp.body)

def format_current(c)
  <<~CUR.strip
    #{c["weatherDesc"][0]["value"]}
    Feels like: #{c["FeelsLikeC"]}°C
    Wind: #{c['windspeedKmph']}Km/h
    Humidity: #{c['humidity']}%
  CUR
end

def format_day(w, i)
  <<~DAY.strip
    #{case i
      when 0
        "<b>Today, #{w["date"]}</b>"
      when 1
        "<b>Tomorrow, #{w["date"]}</b>"
      else
        "<b>#{w["date"]}</b>"
      end}
    ⬆️ #{w['maxtempC']}° ⬇️ #{w['mintempC']}°
    🌅 #{w['astronomy'][0]['sunrise']} 🌇 #{w['astronomy'][0]['sunset']}
    #{w['hourly'].map do |h|
      "#{format_time(h['time'])} #{WEATHER_CODES[h['weatherCode']]} #{format_temp(h['FeelsLikeC'])} #{h['weatherDesc'][0]['value']}, #{format_chances(h)}"
    end.join("\n")}
  DAY
end

puts ({
  text: weather["current_condition"][0].then do |current|
    "#{WEATHER_CODES[current["weatherCode"].to_s]} #{current["FeelsLikeC"]}°C"
  end,
  tooltip: %Q[#{<<~HERE.strip}],
    <b>#{weather["nearest_area"][0]["areaName"][0]["value"]}</b>
    #{weather["current_condition"][0].then { |x| format_current x }}

    #{weather["weather"].each_with_index.map do |w, i| format_day(w, i) end}
  HERE
}).to_json
