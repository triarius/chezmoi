# vim: ft=sh:

gpgconf --launch gpg-agent

unset SSH_AUTH_SOCK
export SSH_AUTH_SOCK
SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)

gpg-connect-agent updatestartuptty /bye &>/dev/null
