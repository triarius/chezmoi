fish_add_path $ZD/kubectl_config/bin/aliases

complete -c kc --wraps kubectl
complete -c kcsudo --wraps kubectl
complete -c kctx --wraps kubie
complete -c kns --wraps kubie

set zendesk_kube_config $XDG_CONFIG_HOME/kube/zendesk.yaml
if not string split : $KUBECONFIG | rg --quiet $zendesk_kube_config
    set KUBECONFIG $zendesk_kube_config:$KUBECONFIG
end
