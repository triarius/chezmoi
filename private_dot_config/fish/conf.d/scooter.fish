# vim: ft=fish

set -x SCOOTER_PROFILE devs/scooter
set -x SCOOTER_ROOT ~/Code/zendesk/scooter
set SCOOTER_CTX scooter-apse2

function _scooter_cli_setup
    mise deactivate
    set -x BUNDLE_GEMFILE $SCOOTER_ROOT/Gemfile
    pushd $SCOOTER_ROOT
    set -lx SCOOTER_GITHUB_TOKEN (pass API/github/triarius/scooter)
end

function _scooter_cli_teardown
    popd
end

function scooter
    _scooter_cli_setup
    direnv exec . /Users/narthana.epa/.local/share/cargo/bin/kubie exec $SCOOTER_CTX default bin/scooter $argv
    _scooter_cli_teardown
end

function scooter-import
    _scooter_cli_setup
    bin/scooter-import $argv
    _scooter_cli_teardown
end
