return {
	{
		"triarius/fileline.nvim",
		-- dir = "/home/narthana/Code/triarius/fileline.nvim",
		config = true,
	},
	{
		"triarius/marks.nvim",
		-- dir = "/home/narthana/Code/triarius/marks.nvim",
		opts = {
			builtin_marks = { ".", "<", ">", "^", "'" },
		},
	},
	"direnv/direnv.vim",
	"pedrohdz/vim-yaml-folds",
	"folke/lsp-colors.nvim",
	"godlygeek/tabular",
	"hashivim/vim-hashicorp-tools",
	"jaxbot/selective-undo.vim",
	"mbbill/undotree",
	"tpope/vim-abolish",
	"tpope/vim-eunuch",
	"tpope/vim-fugitive",
	"tpope/vim-repeat",
	"tpope/vim-sensible",
	"triglav/vim-visual-increment",
	"voldikss/vim-floaterm",
	"wesQ3/vim-windowswap",
	"mzlogin/vim-markdown-toc",
	{
		"rachartier/tiny-glimmer.nvim",
		event = "TextYankPost",
		opts = {},
	},
	{
		"MeanderingProgrammer/render-markdown.nvim",
		dependencies = { "nvim-treesitter/nvim-treesitter", "echasnovski/mini.nvim" }, -- if you use the mini.nvim suite
		opts = {},
	},
	{
		"iamcco/markdown-preview.nvim",
		cmd = { "MarkdownPreviewToggle", "MarkdownPreview", "MarkdownPreviewStop" },
		ft = { "markdown" },
		build = ":call mkdp#util#install()",
	},
	{
		"Duologic/nvim-jsonnet",
		opts = {
			load_lsp_config = true,
			load_dap_config = true,
		},
		config = function(_, opts)
			require("nvim-jsonnet").setup(opts)
		end,
	},
	{
		"yorickpeterse/nvim-tree-pairs",
		opt = {},
	},
	{
		"toppair/peek.nvim",
		event = { "VeryLazy" },
		build = "deno task --quiet build:fast",
		opts = {
			syntax = false,
		},
		config = function(_, opts)
			require("peek").setup(opts)
			vim.api.nvim_create_user_command("PeekOpen", require("peek").open, {})
			vim.api.nvim_create_user_command("PeekClose", require("peek").close, {})
		end,
	},
	{
		"gbprod/cutlass.nvim",
		opts = {
			cut_key = "t",
			override_del = true,
		},
	},
	{
		"akinsho/git-conflict.nvim",
		config = true,
	},
	{
		"chomosuke/typst-preview.nvim",
		ft = "typst",
		version = "0.1.*",
		build = function()
			require("typst-preview").update()
		end,
	},
	{
		"epwalsh/obsidian.nvim",
		lazy = true,
		event = {
			"BufReadPre " .. vim.fn.expand("~") .. "/Documents/obsidian/**.md",
			"BufNewFile " .. vim.fn.expand("~") .. "/Documents/obsidian/**.md",
		},
		dependencies = { "nvim-lua/plenary.nvim" },
		config = function()
			local obsidian = require("obsidian")
			obsidian.setup({
				new_notes_location = "current_dir",
				workspaces = {
					{
						name = "work",
						path = vim.fn.expand("~") .. "/Documents/obsidian/vaults/work",
					},
					{
						name = "personal",
						path = vim.fn.expand("~") .. "/Documents/obsidian/vaults/personal",
					},
				},
				daily_notes = {
					folder = "daily",
				},
				templates = {
					subdir = "templates",
					date_format = "%Y-%m-%d-%a",
					time_format = "%H:%M",
				},
				completion = {
					nvim_cmp = true,
					min_chars = 2,
					wiki_link_func = function(opts)
						if opts.id == nil then
							return string.format("[[%s]]", opts.label)
						elseif opts.label ~= opts.id then
							return string.format("[[%s|%s]]", opts.id, opts.label)
						else
							return string.format("[[%s]]", opts.id)
						end
					end,
				},
				mappings = {
					["fo"] = {
						action = function()
							return obsidian.util.gf_passthrough()
						end,
						opts = { noremap = false, expr = true, buffer = true },
					},
					["<leader>ch"] = {
						action = function()
							return obsidian.util.toggle_checkbox()
						end,
						opts = { buffer = true },
					},
					-- Smart action depending on context, either follow link or toggle checkbox.
					["<cr>"] = {
						action = function()
							return obsidian.util.smart_action()
						end,
						opts = { buffer = true },
					},
				},
				---@param url string
				follow_url_func = function(url)
					vim.fn.jobstart({ "open", url }) -- Mac OS
					-- vim.fn.jobstart({ "xdg-open", url }) -- Linux
				end,
			})

			vim.api.nvim_create_autocmd("BufReadPre", {
				pattern = vim.fn.expand("~") .. "/Documents/obsidian/**.md",
				callback = function()
					vim.opt_local.conceallevel = 1
					vim.opt_local.tabstop = 2
					vim.opt_local.shiftwidth = 2
					vim.opt_local.expandtab = true
				end,
			})
		end,
	},
	{
		"chrisgrieser/nvim-recorder",
		dependencies = "rcarriga/nvim-notify",
		opts = {
			mapping = {
				startStopRecording = "qq",
			},
		},
		config = function(_, opts)
			vim.keymap.set("n", "q", "<nop>", { buffer = true })
			require("recorder").setup(opts)
		end,
	},
	{
		"ellisonleao/glow.nvim",
		config = true,
		cmd = "Glow",
	},
	{
		"rebelot/kanagawa.nvim",
		config = function()
			vim.o.termguicolors = true
			vim.cmd.colorscheme("kanagawa")
		end,
	},
	{
		"folke/lazydev.nvim",
		dependencies = {
			{ "Bilal2453/luvit-meta", lazy = true }, -- optional `vim.uv` typings
		},
		ft = "lua", -- only load on lua files
		opts = {
			library = {
				{ path = "luvit-meta/library", words = { "vim%.uv" } },
			},
		},
	},
	{
		"folke/which-key.nvim",
		opts = {},
		config = function(_, opt)
			require("which-key").setup(opt)
		end,
	},
	{
		"rcarriga/nvim-notify",
		dependencies = {
			"rebelot/kanagawa.nvim",
		},
		opts = {
			timeout = 3000,
			render = "compact",
			top_down = false,
		},
	},
	{
		"smjonas/inc-rename.nvim",
		config = true,
	},
	{
		"folke/noice.nvim",
		event = "VimEnter",
		dependencies = {
			"smjonas/inc-rename.nvim",
			"MunifTanjim/nui.nvim",
			"rcarriga/nvim-notify",
			"Saghen/blink.cmp",
		},
		opts = {
			notify = {
				enabled = true,
			},
			lsp = {
				hover = {
					enabled = false,
				},
				signature = {
					enabled = false,
				},
				override = {
					["vim.lsp.util.convert_input_to_markdown_lines"] = true,
					["vim.lsp.util.stylize_markdown"] = true,
					["cmp.entry.get_documentation"] = true,
				},
			},
			presets = {
				bottom_search = false,
				command_palette = true,
				long_message_to_split = true,
				inc_rename = true,
				lsp_doc_border = true,
			},
		},
		config = function(plugin, opts)
			require(string.gsub(plugin.name, ".nvim", "")).setup(opts)
			vim.api.nvim_create_user_command("C", function()
				require("notify").dismiss({
					pending = true,
					silent = true,
				})
			end, {
				nargs = "*",
				desc = "Clear notifications",
			})
		end,
	},
	{
		"Saghen/blink.cmp",
		build = "cargo build --release",
		opts_extend = {
			"sources.completion.enabled_providers",
			"sources.compat",
			"sources.default",
		},
		dependencies = {
			"triarius/friendly-snippets",
			{
				"saghen/blink.compat",
				optional = true, -- make optional so it's only enabled if any extras need it
				opts = {},
			},
		},
		event = "InsertEnter",
		opts = {
			appearance = {
				-- sets the fallback highlight groups to nvim-cmp's highlight groups
				-- useful for when your theme doesn't support blink.cmp
				-- will be removed in a future release, assuming themes add support
				use_nvim_cmp_as_default = false,
				-- set to 'mono' for 'Nerd Font Mono' or 'normal' for 'Nerd Font'
				-- adjusts spacing to ensure icons are aligned
				nerd_font_variant = "mono",
			},
			completion = {
				accept = {
					-- experimental auto-brackets support
					auto_brackets = {
						enabled = true,
					},
				},
				menu = {
					draw = {
						treesitter = { "lsp" },
					},
				},
				documentation = {
					auto_show = true,
					auto_show_delay_ms = 200,
				},
				ghost_text = {
					enabled = vim.g.ai_cmp,
				},
			},
			sources = {
				default = { "lsp", "path", "snippets", "buffer" },
			},
			cmdline = {
				enabled = false,
			},
			keymap = {
				preset = "enter",
				["<Tab>"] = { "select_next", "fallback" },
				["<S-Tab>"] = { "select_prev", "fallback" },
			},
		},
		config = function(_, opts)
			-- setup compat sources
			local enabled = opts.sources.default
			for _, source in ipairs(opts.sources.compat or {}) do
				opts.sources.providers[source] = vim.tbl_deep_extend(
					"force",
					{ name = source, module = "blink.compat.source" },
					opts.sources.providers[source] or {}
				)
				if type(enabled) == "table" and not vim.tbl_contains(enabled, source) then
					table.insert(enabled, source)
				end
			end

			-- Unset custom prop to pass blink.cmp validation
			opts.sources.compat = nil

			-- check if we need to override symbol kinds
			for _, provider in pairs(opts.sources.providers or {}) do
				if provider.kind then
					local CompletionItemKind = require("blink.cmp.types").CompletionItemKind
					local kind_idx = #CompletionItemKind + 1

					CompletionItemKind[kind_idx] = provider.kind
					---@diagnostic disable-next-line: no-unknown
					CompletionItemKind[provider.kind] = kind_idx

					local transform_items = provider.transform_items
					provider.transform_items = function(ctx, items)
						items = transform_items and transform_items(ctx, items) or items
						for _, item in ipairs(items) do
							item.kind = kind_idx or item.kind
						end
						return items
					end

					-- Unset custom prop to pass blink.cmp validation
					provider.kind = nil
				end
			end

			require("blink.cmp").setup(opts)
		end,
	},
	{
		"VonHeikemen/lsp-zero.nvim",
		branch = "v4.x",
		dependencies = {
			-- LSP Support
			{ "neovim/nvim-lspconfig" },
			{
				"williamboman/mason.nvim",
				build = function()
					pcall(vim.cmd, "MasonUpdate")
				end,
			},
			{ "williamboman/mason-lspconfig.nvim" },

			-- Autocompletion
			{ "Saghen/blink.cmp" },
		},
		config = function()
			local lsp_zero = require("lsp-zero")

			local lsp_attach = function(_, bufnr)
				local m = vim.keymap.set
				local opts = { buffer = bufnr, silent = true }

				m("n", "gD", vim.lsp.buf.declaration, opts)
				m("n", "gd", vim.lsp.buf.definition, opts)
				m("n", "K", vim.lsp.buf.hover, opts)
				m("n", "gi", vim.lsp.buf.implementation, opts)
				m("n", "gc", vim.lsp.buf.incoming_calls, opts)
				m("n", "gK", vim.lsp.buf.signature_help, opts)
				m("n", "<leader>wa", vim.lsp.buf.add_workspace_folder, opts)
				m("n", "<leader>wr", vim.lsp.buf.remove_workspace_folder, opts)
				m("n", "<leader>wl", function()
					print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
				end, opts)
				m("n", "<leader>D", vim.lsp.buf.type_definition, opts)
				m("n", "<leader>rn", vim.lsp.buf.rename, opts)
				m("n", "<leader>ca", vim.lsp.buf.code_action, opts)
				m("n", "gr", vim.lsp.buf.references, opts)
				m("n", "g?", vim.diagnostic.open_float, opts)
				m("n", "[d", vim.diagnostic.goto_prev, opts)
				m("n", "]d", vim.diagnostic.goto_next, opts)
				m("n", "<leader>q", vim.diagnostic.setloclist, opts)
			end

			lsp_zero.extend_lspconfig({
				lsp_attach = lsp_attach,
				float_boarder = "rounded",
				sign_text = true,
			})

			local lspconfig = require("lspconfig")
			lspconfig["ruby_lsp"].setup({
				cmd = {
					"direnv",
					"exec",
					".",
					"ruby-lsp",
				},
			})
			lspconfig["gopls"].setup({})

			require("mason").setup({})
			require("mason-lspconfig").setup({
				ensure_installed = {},
				handlers = {
					lsp_zero.default_setup,
					lua_ls = function()
						local lua_opts = lsp_zero.nvim_lua_ls()
						lspconfig.lua_ls.setup(lua_opts)
					end,
					rust_analyzer = function()
						-- use rustaceananvim
					end,
				},
			})
		end,
	},
	{
		"nvimdev/lspsaga.nvim",
		dependencies = {
			"nvim-treesitter/nvim-treesitter",
			"nvim-tree/nvim-web-devicons",
		},
		opts = {},
	},
	{
		"mrcjkb/rustaceanvim",
		version = "^5",
		lazy = false,
	},
	{
		"jay-babu/mason-nvim-dap.nvim",
		dependencies = {
			"williamboman/mason.nvim",
			"mfussenegger/nvim-dap",
			"VonHeikemen/lsp-zero.nvim",
			"igorlfs/nvim-dap-view",
		},
		opts = {
			ensure_installed = { "delve" },
			automatic_installation = true,
			automatic_setup = true,
		},
		config = function(_, opts)
			require("mason-nvim-dap").setup(opts)

			local dap = require("dap")
			vim.keymap.set("n", "<Leader>dc", dap.continue, { desc = "Debug continue" })
			vim.keymap.set("n", "<Leader>dsr", dap.step_over, { desc = "Debug step over" })
			vim.keymap.set("n", "<Leader>dsi", dap.step_into, { desc = "Debug step into" })
			vim.keymap.set("n", "<Leader>dso", dap.step_out, { desc = "Debug step out" })
			vim.keymap.set("n", "<Leader>db", dap.toggle_breakpoint, { desc = "Toggle breakpoint" })
			vim.keymap.set("n", "<Leader>dlp", function()
				dap.set_breakpoint(nil, nil, vim.fn.input("Log point message: "))
			end)
			vim.keymap.set("n", "<Leader>dl", dap.run_last)
			vim.keymap.set("n", "<Leader>dq", function()
				dap.disconnect({ terminateDebuggee = true })
			end)
		end,
	},
	{
		"igorlfs/nvim-dap-view",
		opts = {
			windows = {
				terminal = {
					hide = { "delve" },
				},
			},
		},
		config = function(_, opts)
			local dap_view = require("dap-view")
			dap_view.setup(opts)
			vim.keymap.set("n", "<Leader>dv", dap_view.toggle, { desc = "Toggle dap-view" })
			vim.keymap.set("n", "<Leader>dw", dap_view.add_expr, { desc = "Add debug watch" })
		end,
	},
	{
		"leoluz/nvim-dap-go",
		opts = {},
		config = function(_, opts)
			local dap_go = require("dap-go")
			dap_go.setup(opts)
			vim.keymap.set("n", "<Leader>dt", dap_go.debug_test, { desc = "Debug test" })
			vim.keymap.set("n", "<Leader>dlt", dap_go.debug_last_test, { desc = "Debug last test" })
		end,
	},
	{
		"theHamsta/nvim-dap-virtual-text",
		opts = {},
	},
	{
		"mfussenegger/nvim-lint",
		config = function()
			require("lint").linters_by_ft = {
				markdown = { "vale" },
			}

			vim.api.nvim_create_autocmd({ "BufWritePost" }, {
				callback = function()
					require("lint").try_lint()
				end,
			})
		end,
	},
	{
		"mhartington/formatter.nvim",
		lazy = true,
		event = {
			"BufWritePost",
		},
		config = function()
			require("formatter").setup({
				logging = true,
				log_level = vim.log.levels.WARN,
				filetype = {
					["*"] = { require("formatter.filetypes.any").remove_trailing_whitespace },
					fish = { require("formatter.filetypes.fish").fishindent },
					go = { require("formatter.filetypes.go").gofumpt },
					javascript = { require("formatter.filetypes.javascript").prettierd },
					javascriptreact = { require("formatter.filetypes.javascriptreact").prettierd },
					json = { require("formatter.filetypes.json").prettierd },
					lua = { require("formatter.filetypes.lua").stylua },
					python = { require("formatter.filetypes.python").black },
					ruby = {
						function()
							local rubocop = require("formatter.filetypes.ruby").rubocop()
							rubocop.exe = "direnv"
							rubocop.args = { "exec", ".", "rubocop", unpack(rubocop.args) }
							return rubocop
						end,
					},
					rust = { require("formatter.filetypes.rust").rustfmt },
					typescript = { require("formatter.filetypes.typescript").prettierd },
					typescriptreact = { require("formatter.filetypes.typescriptreact").prettierd },
				},
			})
			local formatter_group = vim.api.nvim_create_augroup("Formatter", {})
			vim.api.nvim_create_autocmd("BufWritePost", {
				group = formatter_group,
				pattern = "*",
				callback = function()
					if not vim.b.disable_autofmt then
						vim.cmd([[FormatWrite]])
					end
				end,
			})
		end,
	},
	{
		"folke/twilight.nvim",
		config = true,
	},
	{
		"echasnovski/mini.nvim",
		dependencies = {
			"stevearc/oil.nvim",
		},
		config = function()
			local starter = require("mini.starter")
			starter.setup({
				items = {
					starter.sections.sessions(1, true),
					{
						name = "FZF",
						section = "Shortcuts",
						action = "lua require('fzf-lua').files()",
					},
					{
						name = "Files",
						section = "Shortcuts",
						action = "lua require('oil.actions').open_cwd.callback()",
					},
					{
						name = "ObsidianToday",
						action = "lua require('obsidian'); vim.cmd('ObsidianToday')",
						section = "Shortcuts",
					},
					starter.sections.builtin_actions(),
					starter.sections.recent_files(5, false),
					starter.sections.recent_files(5, true),
				},
			})
			require("mini.sessions").setup({
				file = "Session.vim",
				verbose = {
					read = true,
					write = true,
					delete = true,
				},
			})
			require("mini.pairs").setup({})
			require("mini.bracketed").setup({})
			require("mini.surround").setup({})
			require("mini.ai").setup({})
			require("mini.bufremove").setup({})
			require("mini.indentscope").setup({})
		end,
	},
	{
		"Wansmer/treesj",
		dependencies = { "nvim-treesitter/nvim-treesitter" },
		config = true,
	},
	{
		"IndianBoy42/tree-sitter-just",
		dependencies = "nvim-treesitter/nvim-treesitter",
		config = true,
		build = ":TSInstall just",
	},
	{
		"sQVe/sort.nvim",
		config = true,
	},
	{
		"nvim-treesitter/nvim-treesitter",
		config = function()
			require("nvim-treesitter.configs").setup({
				ensure_installed = "all",
				ignore_install = { "phpdoc", "swift" },
				sync_install = false,
				highlight = {
					enable = true,
					additional_vim_regex_highlighting = { "markdown" },
				},
				incremental_selection = {
					enable = true,
					keymaps = {
						init_selection = "gnn",
						node_incremental = "grn",
						scope_incremental = "grc",
						node_decremental = "grm",
					},
				},
				indent = {
					enabled = true,
				},
			})
		end,
		build = ":TSUpdate",
	},
	{
		"inkarkat/vim-SearchHighlighting",
		dependencies = "inkarkat/vim-ingo-library",
	},
	{
		"inkarkat/vim-SearchPosition",
		dependencies = "inkarkat/vim-ingo-library",
	},
	{
		"idanarye/vim-merginal",
		dependencies = "tpope/vim-fugitive",
	},
	{
		"rbong/vim-flog",
		dependencies = "tpope/vim-fugitive",
	},
	{
		"fidian/hexmode",
		config = function()
			vim.g.hexmode_patterns = "*.bin,*.exe,*.dat,*.o,*.so"
		end,
	},
	{
		"nvim-lualine/lualine.nvim",
		dependencies = {
			{ "nvim-tree/nvim-web-devicons", opt = true },
			{ "rebelot/kanagawa.nvim" },
		},
		config = function()
			require("lualine").setup({
				options = {
					section_separators = { left = "", right = "" },
					component_separators = { left = "", right = "" },
					theme = "kanagawa",
				},
				sections = {
					lualine_a = {
						{
							"mode",
							fmt = function(str)
								return str:sub(1, 1)
							end,
						},
					},
					lualine_y = {
						"progress",
						{ require("recorder").displaySlots },
					},
					lualine_z = {
						"location",
						{ require("recorder").recordingStatus },
					},
				},
			})
		end,
	},
	{
		"lewis6991/gitsigns.nvim",
		dependencies = "nvim-lua/plenary.nvim",
		config = true,
	},
	{
		"ruifm/gitlinker.nvim",
		dependencies = "nvim-lua/plenary.nvim",
		config = true,
	},
	{
		"https://gitlab.com/ibhagwan/fzf-lua.git",
		commit = "fb94dde9147af859463b9a1a929159b572ee723b",
		dependencies = { "nvim-tree/nvim-web-devicons" },
		config = function()
			local fzf_lua = require("fzf-lua")
			fzf_lua.setup({
				"telescope",
				winopts = {
					preview = {
						default = "bat",
						layout = "vertical",
					},
				},
			})

			local m = vim.keymap.set
			local opts = { silent = true }
			m("n", "<leader>ff", fzf_lua.files, opts)
			m("n", "<leader>fg", fzf_lua.grep_project, opts)
		end,
	},
	{
		"nvim-telescope/telescope.nvim",
		dependencies = {
			{ "nvim-lua/popup.nvim" },
			{ "nvim-lua/plenary.nvim" },
			{ "nvim-telescope/telescope-fzf-native.nvim", build = "make" },
			{ "nvim-telescope/telescope-ui-select.nvim" },
			{ "folke/noice.nvim" },
		},
		config = function()
			local m = vim.keymap.set
			local opts = { silent = true }
			local tb = require("telescope.builtin")

			m("n", "<leader>fb", tb.buffers, opts)
			m("n", "<leader>fh", tb.help_tags, opts)
			m("n", "<leader>ft", tb.treesitter, opts)
			m("n", "<leader>fo", tb.oldfiles, opts)
			m("n", "<leader>fc", tb.commands, opts)
			m("n", "<leader>fm", tb.marks, opts)
			m("n", "<leader>fl", tb.loclist, opts)
			m("n", "<leader>fr", tb.registers, opts)
			m("n", "<leader>fj", tb.jumplist, opts)
			m("n", "<leader>fs", tb.spell_suggest, opts)
			m("n", "<leader>fz", tb.current_buffer_fuzzy_find, opts)
			m("n", "<leader>fd", tb.diagnostics, opts)
			m("n", "<leader>fy", tb.lsp_document_symbols, opts)
			m("n", "<leader>fw", tb.lsp_workspace_symbols, opts)

			local t = require("telescope")

			t.setup({
				defaults = {
					layout_strategy = "vertical",
					layout_config = { width = 0.95 },
					prompt_prefix = "🔎 ",
					vimgrep_arguments = {
						"rg",
						"--color=never",
						"--no-heading",
						"--with-filename",
						"--line-number",
						"--column",
						"--smart-case",
						"--hidden",
					},
					file_ignore_patterns = {
						"%.lock",
						"__pycache__/*",
						"%.sqlite3",
						"%.ipynb",
						"node_modules/*",
						"%.jpg",
						"%.jpeg",
						"%.png",
						"%.svg",
						"%.otf",
						"%.ttf",
						".git/",
						"%.webp",
						".dart_tool/",
						".gradle/",
						".idea/",
						".settings/",
						".vscode/",
						"gradle/",
						"node_modules/",
						"%.pdb",
						"%.dll",
						"%.class",
						"%.exe",
						"%.cache",
						"%.ico",
						"%.pdf",
						"%.dylib",
						"%.jar",
						"%.docx",
						"%.met",
						"smalljre_*/*",
						".vale/",
					},
				},
				pickers = {
					buffers = {
						sort_mru = true,
						ignore_current_buffer = true,
						mappings = {
							i = {
								["<c-d>"] = "delete_buffer",
							},
							n = {
								["<c-d>"] = "delete_buffer",
							},
						},
					},
					find_files = {
						hidden = true,
					},
					grep_string = {
						shorten_path = true,
						only_sort_text = true,
						search = "",
					},
				},
			})

			t.load_extension("fzf")
			t.load_extension("ui-select")
			t.load_extension("noice")
		end,
	},
	{
		"stevearc/oil.nvim",
		dependencies = { "nvim-tree/nvim-web-devicons" },
		opts = {
			default_file_explorer = true,
			columns = {
				"icon",
				"permissions",
				"size",
				"mtime",
			},
			buf_options = {
				buflisted = false,
				bufhidden = "hide",
			},
			win_options = {
				wrap = false,
				signcolumn = "no",
				cursorcolumn = false,
				foldcolumn = "0",
				spell = false,
				list = false,
				conceallevel = 3,
				concealcursor = "nvic",
			},
			delete_to_trash = true,
			skip_confirm_for_simple_edits = false,
			prompt_save_on_select_new_entry = true,
			cleanup_delay_ms = 2000,
			keymaps = {
				["g?"] = "actions.show_help",
				["<CR>"] = "actions.select",
				["<C-s>"] = "actions.select_vsplit",
				["<C-h>"] = "actions.select_split",
				["<C-t>"] = "actions.select_tab",
				["<C-p>"] = "actions.preview",
				["<C-c>"] = "actions.close",
				["<C-l>"] = "actions.refresh",
				["-"] = "actions.parent",
				["_"] = "actions.open_cwd",
				["`"] = "actions.cd",
				["~"] = "actions.tcd",
				["gs"] = "actions.change_sort",
				["gx"] = "actions.open_external",
				["g."] = "actions.toggle_hidden",
			},
			use_default_keymaps = true,
			view_options = {
				show_hidden = true,
				sort = {
					{ "type", "asc" },
					{ "name", "asc" },
				},
			},
			float = {
				padding = 2,
				max_width = 0,
				max_height = 0,
				border = "rounded",
				win_options = {
					winblend = 0,
				},
				override = function(conf)
					return conf
				end,
			},
			preview = {
				max_width = 0.9,
				min_width = { 40, 0.4 },
				width = nil,
				max_height = 0.9,
				min_height = { 5, 0.1 },
				height = nil,
				border = "rounded",
				win_options = {
					winblend = 0,
				},
			},
			progress = {
				max_width = 0.9,
				min_width = { 40, 0.4 },
				width = nil,
				max_height = { 10, 0.9 },
				min_height = { 5, 0.1 },
				height = nil,
				border = "rounded",
				minimized_border = "none",
				win_options = {
					winblend = 0,
				},
			},
		},
		config = function(plugin, opts)
			require(string.gsub(plugin.name, ".nvim", "")).setup(opts)
			vim.keymap.set("n", "-", "<CMD>Oil<CR>", { desc = "Open parent directory" })
		end,
	},
	{ -- remember cursor position
		"farmergreg/vim-lastplace",
		config = function()
			vim.g.lastplace_ignore_buftype = "quickfix,nofile,help,term"
		end,
	},
	{
		"lervag/vimtex",
		lazy = false,
		config = function()
			vim.g.vimtex_view_method = "zathura"
		end,
	},
	{
		"zbirenbaum/copilot.lua",
		cmd = "Copilot",
		event = "InsertEnter",
		opts = {
			suggestion = {
				enabled = true,
				auto_trigger = true,
				debounce = 75,
			},
			filetypes = {
				yaml = true,
				ruby = true,
				go = true,
				rust = true,
				sh = true,
				bash = true,
				markdown = false,
				help = false,
				gitcommit = true,
				typescript = true,
				typescriptreact = true,
				javascript = true,
				javascriptreact = true,
				python = true,
				gitrebase = false,
				hgcommit = false,
				svn = false,
				cvs = false,
				["."] = false,
			},
			copilot_node_command = vim.fn.expand("~") .. "/.local/share/mise/installs/node/18/bin/node",
		},
		config = function(plugin, opts)
			local keymap = {
				accept = "<C-l>",
				accept_word = false,
				accept_line = false,
				next = "<M-]>",
				prev = "<M-[>",
				dismiss = "<C-]>",
			}

			if jit.os == "OSX" then
				keymap = {
					accept = "<D-l>",
					accept_word = false,
					accept_line = false,
					next = "<D-]>",
					prev = "<D-[>",
					dismiss = "<C-]>",
				}
			end

			opts.suggestion.keymap = keymap

			require(string.gsub(plugin.name, ".lua", "")).setup(opts)
		end,
	},
	{
		"scalameta/nvim-metals",
		dependencies = {
			"nvim-lua/plenary.nvim",
		},
		ft = { "scala", "sbt", "java" },
		opts = function()
			local metals_config = require("metals").bare_config()
			metals_config.on_attach = function(client, bufnr)
				-- your on_attach function
			end

			return metals_config
		end,
		config = function(self, metals_config)
			local nvim_metals_group = vim.api.nvim_create_augroup("nvim-metals", { clear = true })
			vim.api.nvim_create_autocmd("FileType", {
				pattern = self.ft,
				callback = function()
					require("metals").initialize_or_attach(metals_config)
				end,
				group = nvim_metals_group,
			})
		end,
	},
}
