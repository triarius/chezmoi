vim.o.secure = true

-- allow switching buffer w/o write
vim.o.hidden = true

-- GUI clipboard
vim.o.clipboard = "unnamedplus"

vim.o.termguicolors = true

-- tabs
vim.o.tabstop = 2
vim.o.shiftwidth = 2
vim.o.smartindent = true
vim.o.expandtab = true

-- show whitespace
vim.o.list = true
vim.o.listchars = "tab:>·,trail:~,extends:>,precedes:<"

-- wrapping indent
vim.o.breakindent = true

-- suggest line length
vim.o.colorcolumn = "80,100"

-- highlight matching braces
vim.o.showmatch = true

-- incremental substitution
vim.o.inccommand = "nosplit"

-- Searching
vim.o.hlsearch = true -- highlight matches
vim.o.incsearch = true -- incremental searching
vim.o.ignorecase = true -- case insensitive search terms
vim.o.smartcase = true -- unless there is at least one capital letter

-- use mouse in all modes
vim.o.mouse = "a"

-- don't show `--INSERT`
vim.o.showmode = false

-- don't show hjkl at bottom
vim.o.showcmd = false

-- better diffs
vim.o.diffopt = "filler,internal,algorithm:histogram,indent-heuristic"

-- folding
vim.o.foldenable = false
vim.o.foldmethod = "expr"
vim.o.foldexpr = "nvim_treesitter#foldexpr()"

-- open new split below and to the right by default
vim.o.splitbelow = true
vim.o.splitright = true

-- persistent undo
vim.o.undofile = true

-- show the title in the terminal window
-- see :help statusline for the syntax
vim.o.title = true
vim.o.titlestring = "%f"

vim.o.shell = "fish"

vim.o.completeopt = "menuone,preview,noinsert,noselect"

vim.o.guifont = "FiraCode Nerd Font:h9"
