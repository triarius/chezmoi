local grp = vim.api.nvim_create_augroup("user", {})

-- close quickfix menu after selecting choice
vim.api.nvim_create_autocmd("FileType", {
	pattern = { "qf" },
	group = grp,
	command = [[nnoremap <buffer> <CR> <CR>:cclose<CR>]],
})

vim.api.nvim_create_autocmd("TermOpen", {
	pattern = ".*",
	group = grp,
	callback = function()
		vim.opt_local.number = false
		vim.opt_local.relativenumber = false
	end,
})

vim.api.nvim_create_autocmd("Filetype", {
	pattern = "python",
	group = grp,
	callback = function()
		vim.opt_local.tabstop = 4
		vim.opt_local.shiftwidth = 4
	end,
})

vim.api.nvim_create_autocmd("Filetype", {
	pattern = "go,gitconfig",
	group = grp,
	callback = function()
		vim.opt_local.expandtab = false
	end,
})

vim.api.nvim_create_autocmd("Filetype", {
	pattern = "text,latex,tex,md,markdown,gitcommit,norg",
	group = grp,
	callback = function()
		vim.opt_local.spell = true
	end,
})

local local_secret_file = function()
	vim.opt_local.swapfile = false
	vim.opt_local.backup = false
	vim.opt_local.undofile = false
end

vim.api.nvim_create_autocmd({ "BufNewFile", "BufRead" }, {
	pattern = "/dev/shm/gopass.*",
	group = grp,
	callback = local_secret_file,
})

vim.api.nvim_create_autocmd({ "BufNewFile", "BufRead" }, {
	pattern = (os.getenv("PASSWORD_STORE_DIR") or (os.getenv("HOME") .. "/.local/share/password-store")) .. "/.*",
	group = grp,
	callback = local_secret_file,
})

vim.api.nvim_create_autocmd("BufReadPost", {
	group = grp,
	pattern = "*",
	callback = function()
		vim.api.nvim_buf_create_user_command(0, "FormatToggle", function()
			vim.b.disable_autofmt = not vim.b.disable_autofmt
		end, {})
	end,
})
