local m = vim.keymap.set

-- disable ex mode, repeat last macro instead
m("n", "Q", "@@")

-- there's no need to yank the pasted over text
m("x", "p", "P")

-- unset the 'last search pattern' register by hitting return
m("n", "<CR>", ":noh<CR><CR>")

-- Make escape work in the Neovim terminal.
m("t", "<C-\\><Esc>", "<C-\\><C-n>")

-- navigate splits
m("n", "<C-h>", "<C-w>h")
m("n", "<C-j>", "<C-w>j")
m("n", "<C-k>", "<C-w>k")
m("n", "<C-l>", "<C-w>l")

m("n", ";", ":")
m("x", ";", ":")
