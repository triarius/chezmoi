{ config, pkgs, ... }: {
  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "narthana";
  home.homeDirectory = "/home/narthana";

  home.packages = [
    pkgs.alacritty
    pkgs.apko
    pkgs.asdf-vm
    pkgs.avizo
    pkgs.awscli2
    pkgs.bat
    pkgs.black
    pkgs.bottom
    pkgs.breeze-gtk
    pkgs.breeze-icons
    pkgs.breeze-qt5
    pkgs.brightnessctl
    pkgs.browserpass
    pkgs.chezmoi
    pkgs.clipman
    pkgs.cosign
    pkgs.delta
    pkgs.diceware
    pkgs.diff-so-fancy
    pkgs.difftastic
    pkgs.direnv
    pkgs.duf
    pkgs.dunst
    pkgs.exa
    pkgs.fd
    pkgs.firefox
    pkgs.fish
    pkgs.fzf
    pkgs.gcc
    pkgs.gh
    pkgs.ginkgo
    pkgs.git
    pkgs.gnome.gnome-tweaks
    pkgs.gnupg
    pkgs.go-migrate
    pkgs.golangci-lint
    pkgs.google-chrome
    pkgs.goreleaser
    pkgs.grpcurl
    pkgs.htmlq
    pkgs.httpie
    pkgs.hub
    pkgs.jq
    pkgs.jsonnet-bundler
    pkgs.jwt-cli
    pkgs.ldns
    pkgs.libappindicator-gtk3
    pkgs.mcfly
    pkgs.neofetch
    pkgs.nerdfonts
    pkgs.node2nix
    pkgs.nodePackages.prettier
    pkgs.nodePackages.uglify-js
    pkgs.noto-fonts
    pkgs.pamixer
    pkgs.pandoc
    pkgs.pass
    pkgs.pavucontrol
    pkgs.pgcli
    pkgs.python310Packages.ipython
    pkgs.qt5ct
    pkgs.rargs
    pkgs.ripgrep
    pkgs.rmlint
    pkgs.rofi-wayland
    pkgs.rustup
    pkgs.saml2aws
    pkgs.sd
    pkgs.sops
    pkgs.sqlite
    pkgs.starship
    pkgs.steampipe
    pkgs.stripe-cli
    pkgs.sway
    pkgs.sway-contrib.grimshot
    pkgs.swayidle
    pkgs.swaynotificationcenter
    pkgs.trash-cli
    pkgs.waybar
    pkgs.wdisplays
    pkgs.wf-recorder
    pkgs.yq
  ];

  fonts.fontconfig.enable = true;

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "22.05";

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;


  programs.neovim = {
    enable = true;
    vimAlias = true;
    viAlias = true;
    withNodeJs = true;
    withPython3 = true;
    withRuby = true;
  };
}
